import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GraphQLModule } from '@nestjs/graphql';
import { TodosModule } from './todos/todos.module';

@Module({
  imports: [
    TodosModule,
    TypeOrmModule.forRoot(),
    GraphQLModule.forRoot({
      installSubscriptionHandlers: true,
      subscriptions: { 'graphql-ws': true, 'subscriptions-transport-ws': true },
      autoSchemaFile: 'schema.graphql',
    }),
  ],
})
export class AppModule {}
