import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import {
  Resolver,
  Query,
  Mutation,
  Subscription,
  ID,
  Args,
  InputType,
  Field,
} from '@nestjs/graphql';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { PubSubEngine } from 'graphql-subscriptions';
import { Todo, TagColor } from './todo.entity';
import { TodoTopics } from './todos.topics';

@InputType()
export class TodoInput implements Partial<Todo> {
  @Field({ nullable: true })
  name?: string;

  @Field({ nullable: true })
  completed?: boolean;

  @Field(() => TagColor, { nullable: true })
  tag?: TagColor;
}

@Injectable()
@Resolver(() => Todo)
export class TodosResolver {
  constructor(
    @InjectRepository(Todo)
    private readonly todoRepository: Repository<Todo>,
    @Inject('PUB_SUB') private pubSub: PubSubEngine
  ) {}

  @Query(() => [Todo])
  async todos(@Args('tag', { nullable: true }) tag?: TagColor) {
    const options: FindManyOptions<Todo> = { order: { createdDate: 'ASC' } };
    if (!tag) return this.todoRepository.find(options);
    return this.todoRepository.find({ ...options, where: { tag } });
  }

  @Query(() => Todo, { nullable: true })
  async todo(@Args('id', { type: () => ID }) id: string) {
    const todo = this.todoRepository.findOne(id);
    if (!todo) throw new NotFoundException(id);
    return todo;
  }

  @Mutation(() => Todo)
  async todoCreate(@Args('todo') input: TodoInput) {
    const newTodo = this.todoRepository.create(input);
    const todo = await this.todoRepository.save(newTodo);
    this.pubSub.publish(TodoTopics.PublishTodoCreated, { todoCreated: todo });
    return todo;
  }

  @Mutation(() => Todo)
  async todoUpdate(
    @Args('id', { type: () => ID }) id: string,
    @Args('todo') input: TodoInput
  ) {
    const currentTodo = await this.todoRepository.findOne(id);
    if (!currentTodo) throw new NotFoundException(id);
    const updatedTodo = { ...currentTodo, ...input };
    const todo = await this.todoRepository.save(updatedTodo);
    this.pubSub.publish(TodoTopics.PublishTodoUpdated, { todoUpdated: todo });
    return todo;
  }

  @Mutation(() => ID)
  async todoDelete(@Args('id', { type: () => ID }) id: string) {
    const currentTodo = await this.todoRepository.findOne(id);
    if (!currentTodo) throw new NotFoundException(id);
    await this.todoRepository.delete(id);
    this.pubSub.publish(TodoTopics.PublishTodoDeleted, { todoDeleted: id });
    return id;
  }

  @Subscription(() => Todo)
  todoCreated() {
    return this.pubSub.asyncIterator(TodoTopics.PublishTodoCreated);
  }

  @Subscription(() => Todo)
  todoUpdated() {
    return this.pubSub.asyncIterator(TodoTopics.PublishTodoUpdated);
  }

  @Subscription(() => ID)
  todoDeleted() {
    return this.pubSub.asyncIterator(TodoTopics.PublishTodoDeleted);
  }
}
