import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Todo } from './todo.entity';
import { TodosResolver } from './todos.resolver';
import { PubSubProvider } from '../common/pubsub.provider';

@Module({
  imports: [TypeOrmModule.forFeature([Todo])],
  providers: [TodosResolver, PubSubProvider],
})
export class TodosModule {}
