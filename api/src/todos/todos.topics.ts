export const TodoTopics = {
  PublishTodoCreated: 'Topic.PublishTodoCreated',
  PublishTodoUpdated: 'Topic.PublishTodoUpdated',
  PublishTodoDeleted: 'Topic.PublishTodoDeleted',
};
