import { Field, ID, ObjectType, registerEnumType } from '@nestjs/graphql';
import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

export enum TagColor {
  RED = 'RED',
  GREEN = 'GREEN',
  BLUE = 'BLUE',
}

registerEnumType(TagColor, { name: 'TagColor' });

@ObjectType()
@Entity({ name: 'todos' })
export class Todo extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  readonly id: string;

  @Field()
  @Column()
  name: string;

  @Field()
  @Column({ type: Boolean, default: false })
  completed: boolean;

  @Field(() => TagColor, { nullable: true })
  @Column({ type: 'enum', enum: TagColor, nullable: true })
  tag: TagColor;

  @Field()
  @CreateDateColumn()
  readonly createdDate: Date;

  @Field()
  @UpdateDateColumn()
  readonly updatedDate: Date;
}
