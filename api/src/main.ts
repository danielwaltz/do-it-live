import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { AppModule } from './app.module';

const API_PORT = parseInt(`${process.env.API_PORT}`, 10) || 4000;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  await app.listen(API_PORT);
  console.log(`Application is running on: ${await app.getUrl()}`);
}
bootstrap();
