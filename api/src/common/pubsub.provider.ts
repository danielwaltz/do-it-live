import { Provider } from '@nestjs/common';
import { RedisPubSub } from 'graphql-redis-subscriptions';
import { ClientOpts, createClient } from 'redis';

const REDIS_HOST = process.env.REDIS_HOST || 'redis';
const REDIS_PORT = parseInt(`${process.env.REDIS_PORT}`, 10) || 6379;

export const PubSubProvider: Provider = {
  provide: 'PUB_SUB',
  useFactory: () => {
    const options: ClientOpts = {
      host: REDIS_HOST,
      port: REDIS_PORT,
    };

    return new RedisPubSub({
      publisher: createClient(options),
      subscriber: createClient(options),
    });
  },
};
