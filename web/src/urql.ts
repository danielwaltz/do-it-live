import { cacheExchange } from '@urql/exchange-graphcache';
import {
  createClient,
  dedupExchange,
  fetchExchange,
  subscriptionExchange,
} from '@urql/vue';
import { createClient as createWSClient } from 'graphql-ws';
import {
  type TodoCreatedSubscription,
  type TodoCreateMutation,
  type TodoDeletedSubscription,
  TodosDocument,
  type TodoUpdatedSubscription,
} from '@/generated/graphql';

const wsClient = createWSClient({ url: 'ws://localhost:5173/graphql' });

const client = createClient({
  url: 'http://localhost:5173/graphql',
  exchanges: [
    dedupExchange,
    cacheExchange({
      updates: {
        Mutation: {
          todoCreate(result: TodoCreateMutation, _args, cache) {
            cache.updateQuery({ query: TodosDocument }, (data) => {
              if (!data?.todos.length) return { todos: [result.todoCreate] };
              return data;
            });
          },
        },
        Subscription: {
          todoCreated(result: TodoCreatedSubscription, _args, cache) {
            cache.updateQuery({ query: TodosDocument }, (data) => {
              if (!data) return null;

              const todo = result.todoCreated;
              const todos = [...data.todos, todo];
              return { ...data, todos };
            });
          },
          todoUpdated(result: TodoUpdatedSubscription, _args, cache) {
            cache.updateQuery({ query: TodosDocument }, (data) => {
              if (!data) return null;

              const todo = result.todoUpdated;
              const todos = data.todos.map((currentTodo) => {
                if (currentTodo.id !== todo.id) return currentTodo;
                return { ...currentTodo, ...todo };
              });

              return { ...data, todos };
            });
          },
          todoDeleted(result: TodoDeletedSubscription, _args, cache) {
            cache.updateQuery({ query: TodosDocument }, (data) => {
              if (!data) return null;

              const id = result.todoDeleted;
              const todos = data.todos.filter((todo) => todo.id !== id);
              return { ...data, todos };
            });
          },
        },
      },
    }),
    fetchExchange,
    subscriptionExchange({
      forwardSubscription: (operation) => ({
        subscribe: (sink) => ({
          unsubscribe: wsClient.subscribe(operation, sink),
        }),
      }),
    }),
  ],
});

export default client;
