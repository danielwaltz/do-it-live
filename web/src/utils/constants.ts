import { defineConstant } from 'ts-define-constant';
import type { TagColor } from '@/generated/graphql';

export const { values: TAG_COLOR_VALUES } = defineConstant<{
  [K in TagColor]: K;
}>({
  BLUE: 'BLUE',
  GREEN: 'GREEN',
  RED: 'RED',
} as const);
