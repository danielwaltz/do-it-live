import 'uno.css';
import { createApp } from 'vue';
import urql from '@urql/vue';
import App from '@/App.vue';
import client from '@/urql';

const app = createApp(App);

app.use(urql, client);

app.mount('#app');
