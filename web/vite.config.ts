import presetIcons from '@unocss/preset-icons';
import vue from '@vitejs/plugin-vue';
import path from 'path';
import { presetUno } from 'unocss';
import unocss from 'unocss/vite';
import { defineConfig } from 'vite';
import codegen from 'vite-plugin-graphql-codegen';

export default defineConfig({
  plugins: [
    vue(),
    unocss({ presets: [presetUno(), presetIcons()] }),
    codegen({
      configOverrideOnStart: { schema: 'schema.graphql' },
      configOverrideOnBuild: { schema: 'schema.graphql' },
    }),
  ],
  server: {
    host: true,
    proxy: {
      '/graphql': {
        target: 'http://api:4000',
        changeOrigin: true,
        ws: true,
      },
    },
  },
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src'),
    },
  },
});
